select source_net, sum(length)
 from Traffic
 where 
  time < TIMESTAMP('2014-04-21 10:00:00')
  and
  time > TIMESTAMP('2014-04-21 05:00:00')
  group by source_net