-- Count

SELECT SQL_NO_CACHE COUNT(*)
FROM `core_instance` inst 
  INNER JOIN `core_event` ev ON (inst.`event_id` = ev.`id`)
  INNER JOIN `core_eventdata` evd ON (ev.`data_id` = evd.`id`)
  INNER JOIN `core_place` place ON (inst.`place_id` = place.`id` )

WHERE (inst.`time` >= '2014-02-15 15:21:02'
       AND inst.`social_time` >= '2014-02-14 00:00:00'
       AND inst.`social_time` < '2015-02-24 00:00:00'
       AND evd.`category_id` = 26
       AND place.`city_id` = 'MSK');


-- Megaboy


SELECT
  DISTINCT
  `core_event`.`id`, `core_event`.`data_id`, `core_event`.`significance`, `core_event`.`release_date`, `core_event`.`first_time`, `core_event`.`last_time`, `core_event`.`city_id`, `core_event`.`on_top`,  `core_event`.`is_3d`, `core_event`.`merged_to_id`, 
  `core_eventdata`.`id`, `core_eventdata`.`title`, `core_eventdata`.`original`, `core_eventdata`.`genre`, `core_eventdata`.`director`, `core_eventdata`.`country`, `core_eventdata`.`starring`, `core_eventdata`.`duration`,  `core_eventdata`.`category_id`, `core_eventdata`.`age_restricted`, `core_eventdata`.`imdb`, `core_eventdata`.`kinopoisk`, `core_eventdata`.`deleted`, `core_eventdata`.`merged_to_id`, 
  `core_category`.`id`, `core_category`.`slug`, `core_category`.`title`, `core_category`.`on_top`, `core_category`.`hidden`
FROM `core_event`
  INNER JOIN `core_eventdata` ON (`core_event`.`data_id` = `core_eventdata`.`id`)
  INNER JOIN `core_category` ON (`core_eventdata`.`category_id` = `core_category`.`id`)
  INNER JOIN `core_instance` ON (`core_event`.`id` = `core_instance`.`event_id`)
WHERE (`core_event`.`city_id` = 'EKT' AND `core_eventdata`.`deleted` = 0 AND core_event.merged_to_id IS NULL AND
       core_eventdata.merged_to_id IS NULL AND `core_eventdata`.`category_id` = 26 AND
       `core_instance`.`time` >= '2013-11-21 15:01:12' AND
       `core_instance`.`social_time` >= '2013-11-21 00:00:00')
ORDER BY `core_event`.`significance` DESC
LIMIT 10;