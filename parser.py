#!/usr/bin/python3

import argparse
import pymysql
from datetime import datetime

TABLE_NAME = "Traffic"
LOGGER_COUNT = 1000

def parse_line(line):
    """
    Line example:
    Apr 21 05:33:18 znick-N53SV IN=eth0.21 OUT= MAC=14:da:e9:c9:a6:38:08:00:27:cf:1e:e7:08:00  SRC=10.23.11.3 DST=10.23.100.41 LEN=52 TOS=00 PREC=0x00 TTL=63 ID=41535 CE DF PROTO=TCP SPT=80 DPT=48732 SEQ=1379248327 ACK=2217593752 WINDOW=227 ACK URGP=0
    """
    chunks = line.strip().split(" ")
    time_chunks = chunks[:3]
    data_chunks = chunks[4:]
    result_data = dict(tuple(kw_pair.split("=", maxsplit=1)) for kw_pair in data_chunks if "=" in kw_pair)
    result_data['TIME'] = datetime.strptime(' '.join(time_chunks), "%b %d %H:%M:%S").replace(2014)
    return result_data

def parse_file(filename):
    with open(filename[0]) as f:
        for line in f:
            yield parse_line(line)

def prepare_data(data):
    def calc_net(ip):
        return ".".join(ip.split(".")[:3] + ["0/24"])
    data["SRC_NET"] = calc_net(data["SRC"])
    data["DST_NET"] = calc_net(data["DST"])
    return data

def load_data_to_mysql(connection, log_lines):
    counter = 0
    start_time = datetime.now()
    temp = []
    counter_dump = 0
    max_dump = 1000
    with connection as cursor:
        for line_data in map(prepare_data, log_lines):
            if counter_dump >= max_dump:
                counter_dump = 0
                insert_line = "INSERT into {TABLE} (time, source_ip, source_net, destionation_ip, destionation_net, length) values {VAL};".format(
                    TABLE=TABLE_NAME,
                    VAL=', '.join(temp)
                )
                cursor.execute(insert_line)
                temp.clear()
                counter += max_dump
                if counter % LOGGER_COUNT == 0:
                    delta =  datetime.now() - start_time

                    print("\rSpeed: {: .0f} inserts per second".format(LOGGER_COUNT / delta.total_seconds()), end='')

                    start_time = datetime.now()
                    counter = 0
            else:
                counter_dump += 1
                temp.append("('{TIME}', '{SRC}', '{SRC_NET}', '{DST}', '{DST_NET}', {LEN})".format(
                    **line_data
                ))

        if temp:
            insert_line = "INSERT into {TABLE} (time, source_ip, source_net, destionation_ip, destionation_net, length) values {VAL};".format(
                TABLE=TABLE_NAME,
                VAL=', '.join(temp)
            )
            with connection as cursor:
                cursor.execute(insert_line)

def main():
    def prepare_args(args):
        prepared = {}
        if args.host is not None:
            prepared['host'] = args.host
        if args.port is not None:
            prepared['port'] = args.port
        if args.user is not None:
            prepared['user'] = args.user
        if args.password is not None:
            prepared['passwd'] = args.password
        if args.socket is not None:
            prepared['unix_socket'] = args.socket 

        prepared['db'] = args.database
        return prepared
    args = parser.parse_args()
    connection = pymysql.connect(**prepare_args(args))
    load_data_to_mysql(connection, parse_file(args.logfile))


parser = argparse.ArgumentParser(description='Imports ulog traffic data in syslogemu into MySQL')
group = parser.add_mutually_exclusive_group()
group.add_argument("--socket", "-s", 
    type=str,
    help="Unix socket to connet to"    
)
group.add_argument("--host", "-H", 
    type=str, 
    help="Host to connect to (have priority on socket)"    
)
parser.add_argument("--port", "-P", 
    type=int,
    help="Port to connect to",
)
parser.add_argument("--database", "-d", 
    type=str, default="ulog",
    help="Name of database",     
)
parser.add_argument("--user", "-u", 
    type=str,
    help="Connect to database as user",     
)
parser.add_argument("--password", "-p", 
    type=str,
    help="Use password while connect",     
)
parser.add_argument("logfile", nargs=1,
    type=str,
    help="File to parse data"
)
if __name__ == '__main__':
    main()
