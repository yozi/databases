
DROP TABLE IF EXISTS Traffic;

create table Traffic (
	id INT PRIMARY KEY AUTO_INCREMENT,
	time DATETIME NOT NULL,
	source_ip VARCHAR(20) NOT NULL,
	source_net VARCHAR(20) NOT NULL,
	destionation_ip VARCHAR(20) NOT NULL,
	destionation_net VARCHAR(20) NOT NULL,
	length INT NOT NULL,

	INDEX `time_idx` (time),
	INDEX `source_net_idx` (source_net),
	INDEX `destionation_net_idx` (destionation_net)
);
